#!/bin/bash
#################################################################################################################################
# setPermission

# TARGET-System
T_VERSION=mage2

# GIT or FILE
T_PULL=GIT
T_STARTFOLDER=/var/www/kaphingst
T_RELPATH=${T_VERSION}-shop

PHPBIN=php5
#################################################################################################################################
# not change this values

echo "Cleaning"

rm -rf ${T_STARTFOLDER}/${T_RELPATH}/var/view_preprocessed/
rm -rf ${T_STARTFOLDER}/${T_RELPATH}/pub/static/adminhtml

rm -rf ${T_STARTFOLDER}/${T_RELPATH}/var/page_cache
rm -rf ${T_STARTFOLDER}/${T_RELPATH}/var/generation
rm -rf ${T_STARTFOLDER}/${T_RELPATH}/var/di
rm -rf ${T_STARTFOLDER}/${T_RELPATH}/var/cache

sudo -u www-data ${T_STARTFOLDER}/${T_RELPATH}/bin/magento setup:upgrade
sudo -u www-data ${T_STARTFOLDER}/${T_RELPATH}/bin/magento setup:di:compile

sudo -u www-data ${T_STARTFOLDER}/${T_RELPATH}/bin/magento setup:static-content:deploy

echo "CHANGE permissions"
find ${T_STARTFOLDER}/${T_RELPATH} -type d -exec chmod 770 {} \;
find ${T_STARTFOLDER}/${T_RELPATH} -type f -exec chmod 660 {} \;
chmod u+x ${T_STARTFOLDER}/${T_RELPATH}/bin/magento

chown -R www-data.www-data ${T_STARTFOLDER}/${T_RELPATH}/

chmod 777 ${T_STARTFOLDER}/${T_RELPATH}/var
chmod -R 777 ${T_STARTFOLDER}/${T_RELPATH}/pub/media
chmod -R 777 ${T_STARTFOLDER}/${T_RELPATH}/pub/static
chmod 777 ${T_STARTFOLDER}/${T_RELPATH}/app/etc

chmod +x ${T_STARTFOLDER}/${T_RELPATH}/*.sh
chown root.root ${T_STARTFOLDER}/${T_RELPATH}/*.sh

echo "READY"
exit