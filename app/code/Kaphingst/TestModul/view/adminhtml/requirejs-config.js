/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            testModulApp:       'Kaphingst_Testmodule/js/app'
        }
    },
    "deps": [
        "js/theme",
        "mage/backend/bootstrap",
        "mage/adminhtml/globals"
    ]
};